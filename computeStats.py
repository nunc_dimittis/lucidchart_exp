import os
import matplotlib.pyplot as plt

dirName = 'MovingPouches'
gauge_list = []
accu_list = []
speed_list = []
for subDir in os.listdir(dirName):
    print (subDir)
    data_id = subDir.split("_")
    counter = 0
    leak_count = 0
    for fileName in os.listdir(dirName+"\\"+subDir):
        leak = fileName.split('.')[0].split('_')[1]
        if leak == 'leak':
            leak_count += 1
        counter += 1
    accu = int((float(leak_count)/float(counter)) * 100)
    if len(data_id) == 1:
        gauge = int(data_id[0][-2:])
        gauge_list.append(gauge)
        accu_list.append(accu)
    # print(accu)

print(gauge_list)
print(accu_list)

plt.plot(gauge_list, accu_list, '-*')
plt.title("Gauge size vs. accuracy at speed 24 ft/min")
plt.ylabel('accuracy')
plt.xlabel('gauge size')
plt.show()
