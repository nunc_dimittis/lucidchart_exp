
"""
   DWF Python Example
   Author:  Digilent, Inc.
   Revision: 10/17/2013

   Requires:
       Python 2.7, numpy, matplotlib
       python-dateutil, pyparsing
"""
from ctypes import *
from dwfconstants import *
import math
import time
import matplotlib.pyplot as plt
import sys
import csv


class ADAPI:

    def __init__(self):
        '''initialize wrapper object'''
        self.hdwf = None
        if sys.platform.startswith("win"):
            dwf = cdll.dwf
        elif sys.platform.startswith("darwin"):
            dwf = cdll.LoadLibrary("/Library/Frameworks/dwf.framework/dwf")
        else:
            dwf = cdll.LoadLibrary("libdwf.so")
        self.dwf = dwf
        self.samples = 1
        self.channel = 0
        self.analog_check_delay = 0.01
        self.initial_delay = 4
        self.digital_check_delay = 0.01

        self.debug = False

    def version(self):
        '''display Digilent waveforms version'''
        dwf = self.dwf
        #print DWF version
        version = create_string_buffer(16)
        dwf.FDwfGetVersion(version)
        print("DWF Version: ",version.value)

    def init(self):
        '''Initialize analog discovery device - call before all other function'''
        #open device
        dwf = self.dwf
        hdwf = c_int()
        if self.debug: print("Opening first device")
        dwf.FDwfDeviceOpen(c_int(-1), byref(hdwf))

        if hdwf.value == hdwfNone.value:
            szerr = create_string_buffer(512)
            dwf.FDwfGetLastErrorMsg(szerr)
            print(szerr.value)
            print("failed to open device")
            quit()
        self.hdwf = hdwf

    def close(self):
        '''close device - call after other functions, or will segmentation fault'''
        dwf = self.dwf
        dwf.FDwfDeviceCloseAll()

    def ad_sample_prep(self, time_length):
        '''prepare analog discovery to sample for the given time length, at
        maximum resolution, of 8000 samples (per call to ad_sample)'''
        if self.hdwf == None or self.dwf == None:
            print('ad_sample_prep ERROR: hdwf:', self.hdwf, 'DIGITALIO0: dwf:', self.dwf)
            return
        sample_time = float(time_length)  # default, 0.0002
        maxsample = 8000  # specs said 16k, looks more like 8k from experiments

        sample_rate =  float(maxsample / sample_time) ## float(2000) #default 20000000.0
        samples = int(sample_rate * sample_time)  #default4000, max 16000
        if self.debug: print('samples: ', samples)
        if samples > 8000 and self.debug: print('TOO MANY SAMPLES,', samples, ', MAXIMUM OF 8000')
        self.samples = samples
        channel = int(self.channel) # orange 0, blue 1

        #declare ctype variables
        sts = c_byte()
        rgdSamples = (c_double * samples)(1.0)
        dwf = self.dwf
        hdwf = self.hdwf

        if self.debug: print("Preparing to read sample...")

        #set up acquisition
        dwf.FDwfAnalogInFrequencySet(hdwf, c_double(sample_rate))
        dwf.FDwfAnalogInBufferSizeSet(hdwf, c_int(samples))
        dwf.FDwfAnalogInChannelEnableSet(hdwf, c_int(channel), c_bool(True))
        dwf.FDwfAnalogInChannelRangeSet(hdwf, c_int(channel), c_double(5))

        #wait at least 2 seconds for the offset to stabilize
        time.sleep(self.initial_delay)

    def ad_sample(self):
        '''start sampling with the settings specified in ad_sample_prep'''
        if self.hdwf == None or self.dwf == None:
            print('ad_sample ERROR: hdwf:', self.hdwf, 'DIGITALIO0: dwf:', self.dwf)
            return
        dwf = self.dwf
        hdwf = self.hdwf
        samples = self.samples
        channel = self.channel

        sts = c_byte()
        rgdSamples = (c_double * samples)(1.0)

        #begin acquisition
        dwf.FDwfAnalogInConfigure(hdwf, c_bool(False), c_bool(True))
        if self.debug: print("waiting to finish")

        while True:
            dwf.FDwfAnalogInStatus(hdwf, c_int(1), byref(sts))
            if self.debug: print("STS VAL: ", str(sts.value), "STS DONE: ", str(DwfStateDone.value))
            if sts.value == DwfStateDone.value :
                break
            time.sleep(self.analog_check_delay)
        if self.debug: print("Acquisition finished")

        dwf.FDwfAnalogInStatusData(hdwf, channel, rgdSamples, samples) # get channel 1 data
        #dwf.FDwfAnalogInStatusData(hdwf, 1, rgdSamples, 4000) # get channel 2 data
        return rgdSamples


    def digitalIO0(self):
        '''return the state of the 0 digital IO pin'''
        if self.hdwf == None or self.dwf == None:
            print('DIGITALIO0 ERROR: hdwf:', self.hdwf, 'DIGITALIO0: dwf:', self.dwf)
            return
        dwf = self.dwf
        hdwf = self.hdwf
        dwRead = c_uint32()
        #print("Preparing to read Digital IO pins...")
        # fetch digital IO information from the device
        dwf.FDwfDigitalIOStatus (hdwf)
        # read state of all pins, regardless of output enable
        dwf.FDwfDigitalIOInputStatus(hdwf, byref(dwRead))

        #print dwRead as bitfield (32 digits, removing 0b at the front)
        s = str(bin(dwRead.value)[2:].zfill(32))
        #print(s)
        on = (s[-1] == '1')
        # print('DIO 0: ', on)
        return on

    def poll_digitalIO0(self, edge):
        '''blocking call to wait for a digital high signal on digital pin 0'''
        if self.hdwf == None or self.dwf == None:
            print('poll_digitalIO0 ERROR: hdwf:', self.hdwf, 'DIGITALIO0: dwf:', self.dwf)
            return
        while self.digitalIO0() != edge:
            time.sleep(self.digital_check_delay)
        return True


    def digitalO1(self, on):
        '''sets the digital output on pin 1 to high or low depending on the value
        of on'''
        if self.hdwf == None or self.dwf == None:
            print('DIGITALIO0 ERROR: hdwf:', self.hdwf, 'DIGITALIO0: dwf:', self.dwf)
            return
        dwf = self.dwf
        hdwf = self.hdwf
        # enable output/mask on 8 LSB IO pins, 7654321
        #                                      0000010
        dwf.FDwfDigitalIOOutputEnableSet(hdwf, c_int(0x0002))
        # set value on enabled IO pins
        if on:
            dwf.FDwfDigitalIOOutputSet(hdwf, c_int(0x0002))
            return True
        else:
            dwf.FDwfDigitalIOOutputSet(hdwf, c_int(0x0000))
            return False

    def dc_out(self, offset, channel=0):
        '''output the specified DC voltage'''
        if offset > 5 or offset < -5: offset = 0
        dwf = self.dwf
        hdwf = self.hdwf
        if self.hdwf == None or self.dwf == None:
            print('dc_out ERROR: hdwf:', self.hdwf, 'dc_out: dwf:', self.dwf)
            return
        dwf.FDwfAnalogOutNodeEnableSet(hdwf, channel, AnalogOutNodeCarrier, c_bool(True))
        dwf.FDwfAnalogOutNodeFunctionSet(hdwf, channel, AnalogOutNodeCarrier, funcDC)
        dwf.FDwfAnalogOutNodeOffsetSet(hdwf, channel, AnalogOutNodeCarrier, c_double(offset))

        print("outputting DC voltage at ", offset)
        dwf.FDwfAnalogOutConfigure(hdwf, channel, c_bool(True))

    def write_samples(self, rgdSamples, file):
        '''write a python list to a csv file'''
        f = open(file, 'w+')
        # f = open('currPouch.csv', 'w+')
        w = csv.writer(f, lineterminator='\n')
        for s in rgdSamples:
            #print(s)
            w.writerow([str(s)])

    def plot_samples(self, rgdSamples):
        '''plot a python list with matplotlib'''
        import matplotlib.pyplot as plt

        f = open('currPouch.csv', 'r+')
        r = csv.reader(f)
        rgdSamples = []
        for row in r:
            rgdSamples += [float(row[0])]

        if self.debug: print(rgdSamples)

        #plot window
        dc = sum(rgdSamples)/len(rgdSamples)
        if self.debug:
            print("DC: ", str(dc), "V")
            print("samples")
            for s in rgdSamples:
                print(s)

        rgpy=[0.0]*len(rgdSamples)
        for i in range(0,len(rgpy)):
            rgpy[i]=rgdSamples[i]

        plt.plot(rgpy)
        plt.show()



''' testing and main functions'''

def dc_out_test():
    ad = ADAPI()
    ad.init()
    ad.dc_out(2)
    time.sleep(3)
    ad.dc_out(3)
    time.sleep(3)
    ad.dc_out(4)
    time.sleep(3)
    ad.dc_out(-3)
    time.sleep(3)
    ad.dc_out(-5)
    time.sleep(3)
    ad.dc_out(2.8)
    time.sleep(3)
    ad.close()

def digital_out_test():
    ad = ADAPI()
    ad.init()
    times = []
    if ad.hdwf == None or ad.dwf == None:
        print('ERROR: hdwf:', ad.hdwf, 'DIGITALIO0: dwf:', ad.dwf)
        return
    for i in range(50):
        st = time.time()
        b = ad.digitalO1(True)
        ft = time.time() - st
        print('digital check time: ', ft, 'value: ', b)
        times += [ft]
    input('continue...')
    for i in range(50):
        st = time.time()
        b = ad.digitalO1(False)
        ft = time.time() - st
        print('digital check time: ', ft, 'value: ', b)
        times += [ft]
    for tm in times:
        print(tm)
    print('average time: ', sum(times)/len(times))
    print('max time: ', max(times))
    ad.close()



def digital_test():
    ad = ADAPI()
    ad.init()
    times = []
    if ad.hdwf == None or ad.dwf == None:
        print('ERROR: hdwf:', ad.hdwf, 'DIGITALIO0: dwf:', ad.dwf)
        return
    for i in range(50):
        st = time.time()
        b = ad.digitalIO0()
        ft = time.time() - st
        print('digital check time: ', ft, 'value: ', b)
        times += [ft]
    input('continue...')
    for i in range(50):
        st = time.time()
        b = ad.digitalIO0()
        ft = time.time() - st
        print('digital check time: ', ft, 'value: ', b)
        times += [ft]
    for tm in times:
        print(tm)
    ad.close()

def analog_test():
    ad = ADAPI()
    ad.init()
    times = []
    if ad.hdwf == None or ad.dwf == None:
        print('ERROR: hdwf:', ad.hdwf, 'dwf: dwf:', ad.dwf)
        return
    t = 3.1
    print('initializing...')
    ad.ad_sample_prep(t)
    print('initialized.')
    for i in range(100):
        st = time.time()
        s = ad.ad_sample()
        ft = time.time() - st
        ft = ft - t
        print('analog sample time overhead: ', ft, 'value: ', len(s))
        times += [ft]
    for tm in times:
        print(tm)
    print('average time: ', sum(times)/len(times))
    print('max time: ', max(times))
    ad.close()


def test():
    ad = ADAPI()
    ad.init()
    t = 1.1
    print('initializing...')
    ad.ad_sample_prep(t)
    print('initialized.')
    c = input('continue? (n)')
    while(c != 'n'):
        print('waiting for digital input')
        ad.poll_digitalIO0()
        # st = time.time()
        s = ad.ad_sample()
        # ft = time.time() - st
        print('time taken without sampling time: ', ft - t)
        ad.write_samples(s)
        ad.plot_samples(s)
        c = input('continue? (n)')
    ad.close()



def main():
    ad = ADAPI()
    ad.init()
    ad.ad_sample_prep(1.1)
    print('waiting for digital signal to start sampling')
    ad.poll_digitalIO0()
    s = []
    for i in range(3):
        s += ad.ad_sample()
    ad.write_samples(s)
    ad.close()
    ad.plot_samples(s)

# test()
