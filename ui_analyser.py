# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'scanning.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtWidgets import QMessageBox, QDialog, QDesktopWidget, QMainWindow, QFileDialog

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from ui_infoInput import *
import pandas as pd
import matplotlib.pyplot as plt

import random

from backend import *
import analogDiscovery as ad

class Ui_Analyser(QMainWindow):
    status = 0
    emergencyNum = 4
    start = False

    df = pd.DataFrame([[0, 0]], columns=['Timestemp', 'Leak'])
    counter = 0

    def setupUi(self, Analyser):
        Analyser.setObjectName("Analyser")
        Analyser.resize(800, 600)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Analyser.sizePolicy().hasHeightForWidth())
        Analyser.setSizePolicy(sizePolicy)
        Analyser.setUnifiedTitleAndToolBarOnMac(False)
        self.centralwidget = QtWidgets.QWidget(Analyser)
        self.centralwidget.setObjectName("centralwidget")

        # Progress bar
        self.progressValue = 0
        self.progressBar = QtWidgets.QProgressBar(self.centralwidget)
        self.progressBar.setGeometry(QtCore.QRect(40, 420, 441, 23))
        self.progressBar.setProperty("value", self.progressValue)
        self.progressBar.setObjectName("progressBar")

        self.scanning = QtWidgets.QLabel(self.centralwidget)
        self.scanning.setGeometry(QtCore.QRect(40, 370, 251, 31))
        font = QtGui.QFont()
        font.setFamily("Monospace")
        font.setPointSize(17)
        font.setBold(True)
        font.setWeight(75)
        self.scanning.setFont(font)
        self.scanning.setTextFormat(QtCore.Qt.PlainText)
        self.scanning.setObjectName("scanning")


        self.status = QtWidgets.QLabel(self.centralwidget)
        self.status.setGeometry(QtCore.QRect(100, 355, 221, 60))
        self.status.setObjectName("status")

        self.leak = QtWidgets.QLabel(self.centralwidget)
        self.leak.setGeometry(QtCore.QRect(200, 363, 221, 31))
        self.leak.setFont(font)
        self.leak.setTextFormat(QtCore.Qt.PlainText)
        self.leak.setObjectName("leak")
        self.leak.hide()

        # plot product signal
        self.plot_widget = QtWidgets.QWidget(self.centralwidget)
        self.plot_widget.setGeometry(50, -110, 800, 600)

        l = QtWidgets.QVBoxLayout()
        # l.setGeometry(QtCore.QRect(50, 30, 391, 321))
        #sc = MyStaticMplCanvas(self.main_widget, width=5, height=4, dpi=100)
        self.dc = MyDynamicMplCanvas(self.plot_widget, width=4, height=3, dpi=100)
        #l.addWidget(sc)
        l.addWidget(self.dc)
        self.plot_widget.setLayout(l)

        # right panel
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(520, 10, 271, 561))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")

        # display current time
        self.curTime = QtWidgets.QLabel(self.frame)
        self.curTime.setGeometry(QtCore.QRect(0, 520, 271, 31))
        self.curTime.setAlignment(QtCore.Qt.AlignCenter)
        self.curTime.setObjectName("curTime")


        # Product Info
        self.productInfo = QtWidgets.QLabel(self.frame)
        self.productInfo.setGeometry(QtCore.QRect(10, 10, 271, 31))
        font = QtGui.QFont()
        font.setFamily("Monospace")
        font.setPointSize(12)
        font.setBold(True)
        font.setUnderline(False)
        font.setWeight(75)
        self.productInfo.setFont(font)
        self.productInfo.setObjectName("productInfo")

        self.productName = QtWidgets.QLabel(self.frame)
        self.productName.setGeometry(QtCore.QRect(30, 50, 271, 31))
        self.productName.setObjectName("productName")
        self.productName.setFont(font)

        self.systemInfo = QtWidgets.QLabel(self.frame)
        self.systemInfo.setGeometry(QtCore.QRect(10, 110, 271, 31))
        self.systemInfo.setFont(font)
        self.systemInfo.setObjectName("systemInfo")

        self.curVolt = QtWidgets.QLabel(self.frame)
        self.curVolt.setGeometry(QtCore.QRect(30, 150, 271, 17))
        self.curVolt.setObjectName("curVolt")

        self.lineRate = QtWidgets.QLabel(self.frame)
        self.lineRate.setGeometry(QtCore.QRect(30, 170, 271, 40))
        self.lineRate.setObjectName("lineRate")

        self.lineRateBtn = QtWidgets.QPushButton(self.frame)
        self.lineRateBtn.setGeometry(QtCore.QRect(45, 220, 200, 31))
        self.lineRateBtn.setAutoFillBackground(False)
        self.lineRateBtn.setObjectName("lineRateBtn")
        self.lineRateBtn.clicked.connect(self.lineRateSetting)


        # emergency setting button
        self.emergencyInfo = QtWidgets.QLabel(self.frame)
        self.emergencyInfo.setGeometry(QtCore.QRect(20, 300, 271, 60))
        self.emergencyInfo.setObjectName("emergencyInfo")
        self.emergencyInfo.setFont(font)

        self.emergencyBtn = QtWidgets.QPushButton(self.frame)
        self.emergencyBtn.setGeometry(QtCore.QRect(45, 370, 200, 31))
        self.emergencyBtn.setAutoFillBackground(False)
        self.emergencyBtn.setObjectName("emergencyBtn")
        self.emergencyBtn.clicked.connect(self.emergencySetting)

        self.pauseBtn = QtWidgets.QPushButton(self.centralwidget)
        self.pauseBtn.setGeometry(QtCore.QRect(50, 490, 101, 31))
        self.pauseBtn.setObjectName("pauseBtn")
        self.pauseBtn.clicked.connect(self.pause_timer)

        self.restartBtn = QtWidgets.QPushButton(self.centralwidget)
        self.restartBtn.setGeometry(QtCore.QRect(160, 490, 200, 31))
        self.restartBtn.setObjectName("restartBtn")
        self.restartBtn.clicked.connect(self.openInfoInput)

        self.stopBtn = QtWidgets.QPushButton(self.centralwidget)
        self.stopBtn.setGeometry(QtCore.QRect(370, 490, 101, 31))
        self.stopBtn.setObjectName("stopBtn")
        self.stopBtn.clicked.connect(self.save_file)

        Analyser.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(Analyser)
        self.statusbar.setObjectName("statusbar")
        Analyser.setStatusBar(self.statusbar)

        self.retranslateUi(Analyser)
        QtCore.QMetaObject.connectSlotsByName(Analyser)
        self.rate = 3

        # # simulate delay for testing
        self.timer = QtCore.QTimer(self)
        # self.timer.timeout.connect(self.toggleDiaplay)
        self.timer.start(4000)

        self.progress_timer = QtCore.QTimer(self)
        self.progress_timer.timeout.connect(self.updateProgress)
        self.progress_timer.start(30)

        self.progressbar_timer = QtCore.QTimer(self)
        self.progressbar_timer.timeout.connect(self.updateProgressBar)
        # self.progressbar_timer.start((self.rate * 1000) / 1000)
        self.progressbar_timer.start(40)

        self.csv_file = "product.csv"




        # self.pauseBtn.clicked.connect(self.toggleLabel)

    def save_file(self):
        self.timer.stop()
        self.progress_timer.stop()
        buttonReply = QMessageBox.question(self,
                                           'Program paused',
                                           "This will quit the current process. \n Current stats saved in " + self.csv_file
                                            + "\n Save to a new location?",
                                           QMessageBox.Yes, QMessageBox.No)
        if buttonReply == QMessageBox.Yes:
            print('Yes clicked.')
            self.saveFileDialog()
        self.analog_discovery.close()
        analyser.hide()

    def saveFileDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getSaveFileName(self,"QFileDialog.getSaveFileName()","","All Files (*);;csv Files (*.csv)", options=options)
        if fileName:
            print(fileName)
            Ui_Analyser.df.to_csv(fileName)
        QtCore.QCoreApplication.instance().quit()

    def openInfoInput(self):
        self.timer.stop()
        self.progress_timer.stop()
        buttonReply = QMessageBox.question(self,
                                           'Program paused',
                                           "This will quit the current process. Proceed?",
                                           QMessageBox.Yes, QMessageBox.No)
        if buttonReply == QMessageBox.Yes:
            print('Yes clicked.')
            self.window = QtWidgets.QMainWindow()
            self.analyser = Ui_MainWindow()
            self.analyser.setupUi(self.window)
            analyser.hide()
            self.window.show()

    def pause_timer(self):
        self.timer.stop()
        self.progress_timer.stop()
        self.resume_qbox()

    def resume_qbox(self):
        buttonReply = QMessageBox.question(self,
                                           'Program paused',
                                           "Resume?",
                                           QMessageBox.Yes, QMessageBox.No)
        if buttonReply == QMessageBox.Yes:
            print('Yes clicked.')
            self.resume_timer()

    def resume_timer(self):
        self.timer.start()
        self.progress_timer.start()

    def lineRateSetting(self):
        self.timer.stop()
        self.progress_timer.stop()
        i, okPressed = QtWidgets.QInputDialog.getInt(self, "Line Rate Setting", "current line rate ft/sec:", 0.1, 0.1, 1 ,0.05)
        if okPressed:
            print(i)
            self.resume_timer()
            self.emergencyInfo.setText(str(Ui_Analyser.emergencyNum) + " leaky pouches \n until emergency shutoff")
            self.lineRate.setText("Line Rate: " + str(i))


    def emergencySetting(self):
        self.timer.stop()
        self.progress_timer.stop()
        i, okPressed = QtWidgets.QInputDialog.getInt(self, "Emergency Setting", "# of leaky pouches:", 4, 0, 10, 1)
        if okPressed:
            print(i)
            self.resume_timer()
            Ui_Analyser.emergencyNum = int(i)
            self.emergencyInfo.setText(str(Ui_Analyser.emergencyNum) + " leaky pouches \n until emergency shutoff")
        self.analog_discovery.close()

    def emergency_response(self):
        self.timer.stop()
        self.progress_timer.stop()
        self.analog_discovery.digitalO1(False)



        buttonReply = QMessageBox.question(self,
                                           'EMERGENCY!',
                                           "Signal sent to voltage supply. \n Press emergency stop button if needed!",
                                           QMessageBox.Ok, QMessageBox.No)
        QtCore.QCoreApplication.instance().quit()

    def updateProgressBar(self):
        if self.progressValue < 100:
            self.progressValue += 1
            self.progressBar.setValue(self.progressValue)

    def updateProgress(self):
        # Read from analog discovery and update progress
        import analogDiscovery as ad
        # print("hello" + str(ad.digitalIO0()))
        # print("digitalIO0 is " + str(self.analog_discovery.digitalIO0()))
        print("poll digitalIO0 is " + str(self.analog_discovery.poll_digitalIO0(True)))
        # if self.progressValue < 100:
        #     self.progressValue += 1
        #     self.progressBar.setValue(self.progressValue)
        # if Ui_Analyser.start == False:

        if (self.analog_discovery.poll_digitalIO0(True) == True):
            self.analog_discovery.ad_sample_prep(self.rate)
            Ui_Analyser.start = True

        # else:
            self.timer = QtCore.QTimer(self)
            self.timer.setSingleShot(True)
            self.timer.timeout.connect(self.toggleDiaplay)
            self.timer.start(self.rate * 1000)
            # self.toggleDiaplay()
            print ("digitalIO0 is returning true")



        # update time
        self.curTime.setText(QtCore.QDateTime.currentDateTime().toString())

        if (Ui_Analyser.counter == self.emergencyNum):
            self.emergency_response()


    def toggleDiaplay(self):

        # reset progress bar
        self.progressValue = 0
        self.progressBar.setValue(self.progressValue)
        # self.progressbar_timer = QtCore.QTimer(self)
        # self.progressbar_timer.timeout.connect(self.updateProgressBar)
        # self.progressbar_timer.start((self.rate * 1000) / 100)


        # analyse data and update display
        t, v = read_csv()
        y = reduce_noise(t, v)
        self.dc.update_figure(t, y)

        pks = find_peaks(y)
        vys = find_valleys(y)

        # three status:
        # 0 - Scanning
        # 1 - Leak
        # 2 - Pass
        prev_status = Ui_Analyser.status
        isleak = isLeak(t, y, pks, vys)
        if (isleak):
            status = 1
        else:
            status = 2
        new_df = pd.DataFrame([[QtCore.QDateTime.currentDateTime().toString(), str(isleak)]],
                              columns=['Timestemp', 'Leak'])
        Ui_Analyser.df = Ui_Analyser.df.append(new_df)

        if prev_status != status:
            if status == 1:
                self.scanning.hide()
                self.leak.setText("Leak!")
                self.leak.show()
                pixmap = QPixmap('thumbdown.png')
                pixmap = pixmap.scaled(50, 50)
                self.status.setPixmap(pixmap)
                self.resize(pixmap.width(), pixmap.height())
                self.status.show()
                Ui_Analyser.counter += 1
            elif status == 2:
                self.scanning.hide()
                self.leak.setText("Pass")
                self.leak.show()
                pixmap = QPixmap('thumbup.png')
                pixmap = pixmap.scaled(50, 50)
                self.status.setPixmap(pixmap)
                Ui_Analyser.counter = 0
        Ui_Analyser.df.to_csv(self.csv_file+"", mode='a')


        # timerScreen = QtCore.QTimer(self)
        # timerScreen.setInterval(500)
        # timerScreen.setSingleShot(True)
        # timerScreen.timeout.connect(self.back_to_scanning)

    def back_to_scanning(self):
        print("in back to scanning")
        self.scanning.show()
        self.leak.hide()
        self.status.hide()
        Ui_Analyser.status = 0

    def set_parameters(self, product, voltage, linerate):
        print("in set parameters")
        self.productName.setText(product)
        self.name = product
        self.curVolt.setText("Current Voltage: " + voltage)
        self.lineRate.setText("Line Rate: " + linerate)
        self.rate = float(linerate)
        self.csv_file = product + ".csv"

    def retranslateUi(self, Scanning):
        _translate = QtCore.QCoreApplication.translate
        Scanning.setWindowTitle(_translate("MainWindow", "Furmano\'s Inline Leak Detector"))
        self.scanning.setText(_translate("MainWindow", "Scanning..."))
        self.leak.setText(_translate("MainWindow", "LEAK!"))
        # self.status.setText(_translate("MainWindow", ""))
        self.curTime.setText(_translate("MainWindow", "Current time"))
        self.productInfo.setText(_translate("MainWindow", "Product Info:"))
        self.systemInfo.setText(_translate("MainWindow", "System Info:"))
        self.curVolt.setText(_translate("MainWindow", "Current Voltage:"))
        self.lineRate.setText(_translate("MainWindow", "Line Rate"))
        self.emergencyInfo.setText(_translate("MainWindow", str(Ui_Analyser.emergencyNum) + " leaky pouches \n until emergency shutoff"))
        self.productName.setText(_translate("MainWindow", "Tomato"))
        self.pauseBtn.setText(_translate("MainWindow", "Pause"))
        self.restartBtn.setText(_translate("MainWindow", "Change Product"))
        self.stopBtn.setText(_translate("MainWindow", "STOP"))
        self.emergencyBtn.setText(_translate("MainWindow", "Emergency Settings"))
        self.lineRateBtn.setText(_translate("MainWindow", "Line Rate Settings"))

    def setup_ad(self):
        my_ad = ad.ADAPI()

        my_ad.init()
        # set analog discovery with 2.8 dc output
        my_ad.dc_out(3.75)
        my_ad.digitalO1(True)
        self.analog_discovery = my_ad

class MyMplCanvas(FigureCanvas):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""

    def __init__(self, parent=None, width=6, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)

        self.compute_initial_figure()

        FigureCanvas.__init__(self, fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                                   QtWidgets.QSizePolicy.Fixed,
                                   QtWidgets.QSizePolicy.Fixed)
        FigureCanvas.updateGeometry(self)

    def compute_initial_figure(self):
        pass


class MyDynamicMplCanvas(MyMplCanvas):
    """A canvas that updates itself every second with a new plot."""

    def __init__(self, *args, **kwargs):
        MyMplCanvas.__init__(self, *args, **kwargs)
        # timer = QtCore.QTimer(self)
        # timer.timeout.connect(self.update_figure)
        # timer.start(2500)

    def compute_initial_figure(self):

        y = np.ones(1000)

        self.axes.plot(y, 'r')

    def update_figure(self, x, y):
        self.axes.cla()
        # x, y = read_csv()
        self.axes.plot(x, y, 'r')
        self.draw()

if __name__ == "__main__":
    import sys
    #Set up Analog Discovery
    import analogDiscovery as ad
    # my_ad = ad.ADAPI()
    #
    # my_ad.init()
    # # set analog discovery with 2.8 dc output
    # my_ad.dc_out(2.8)
    # my_ad.digitalO1(True)

    app = QtWidgets.QApplication(sys.argv)
    analyser = QtWidgets.QMainWindow()
    ui = Ui_Analyser()

    ui.setup_ad()
    ui.setupUi(analyser)

    analyser.show()
    sys.exit(app.exec_())
