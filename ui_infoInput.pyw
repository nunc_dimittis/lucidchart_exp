# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'InfoInput.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QMessageBox, QDialog, QDesktopWidget, QMainWindow, QWidget, QInputDialog
from ui_analyser_new import *
# import analogDiscovery as ad

class Ui_MainWindow(QWidget):

    def __init__(self):
        super().__init__()
        self.title = 'Furmano\'s inline leak detector'
        self.left = 10
        self.top = 10
        self.width = 640
        self.height = 480
        self.setupUi()

    def searchProduct(self, product):

        voltage = "5 kV"
        # linerate = "4 sec/pouch\n         15 pouch/min"
        linerate = "4"
        self.openAnalyser(product, voltage, linerate)


    def getChoice(self):
        items = ("Tomato", "Bean")
        item, okPressed = QInputDialog.getItem(self, "Select product", "Please choose a product:", items, 0, False)
        if okPressed and item:
            print(item)
            self.searchProduct(item)


    def openAnalyser(self, product, voltage, linerate):
        print("in open analyser")
        self.window = QtWidgets.QMainWindow()
        self.analyser = Ui_Analyser()
        self.analyser.setup_ad()
        self.analyser.setupUi(self.window)
        self.analyser.set_parameters(product, voltage, linerate)


    def setupUi(self):

        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.getChoice()

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Furmano\'s Inline Leak Detector"))
        self.textEdit.setPlaceholderText(_translate("Dialog", "Please Enter Product Code"))
        self.searchBtn.setText(_translate("MainWindow", "Search"))
        self.defaultBtn.setText(_translate("MainWindow", "Use Default"))

if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    # ui.setupUi()
    # MainWindow.show()
    sys.exit(app.exec_())
