import backend as be
import matplotlib.pyplot as plt
import glob
import os


allFiles = glob.glob('M8Samples/BakedBeansNonLeak/*.csv')


def isLeak(t, y, pks, vys):
    if pks.size != 0:
        for pk in pks:
            vys = [i for i in vys if i > pk]
            if len(vys) == 0:
                vys.append(len(y) - 1)

            # if pk <= vys[0]:
            #     pass
            else:
                t1 = t[pk:vys[0]]
                y1 = y[pk:vys[0]]
                print(str(pk) + " ," + str(vys[0]))
                peak_width = t[vys[0]] - t[pk]
                print(peak_width)
                if peak_width <= 1900 and peak_width > 200:
                    return True
    return False

if __name__ == '__main__':
    statsFile = open("stats.txt", "w+")
    leakcounter = 0
    nonleakcounter =0 
    for f in allFiles:

        filename = os.path.splitext(os.path.basename(f))[0]
        print(filename)
        # f = "samples/bean13.csv"

        t, v = be.read_csv(f)
        y = be.reduce_noise(t, v)
        pks = be.find_peaks(y)
        vys = be.find_valleys(y)

        # three status:
        # 0 - Scanning
        # 1 - Leak
        # 2 - Pass
        # if be.isLeak(t, y, pks, vys):
        #     print ("Leak")
        # else:
        #     print ("Non-Leak")
        label = "Unknown"

        if be.isLeak(t, y, pks, vys):
            label = "Leak"
            statsFile.write("Leak,")
            leakcounter+=1
            print("Leak")
        else:
            label = "Non-Leak"
            statsFile.write("Non-Leak,")
            nonleakcounter+=1
            print("Non-Leak")

        _, ax = plt.subplots(1, 1)
        # plt.plot(t, v)
        ax.plot(pks, y[pks], '+', mfc=None, mec='r', mew=2, ms=8,
                label='%d %s' % (pks.size, "peak"))
        ax.plot(vys, y[vys], '*', mfc=None, mec='g', mew=2, ms=8,
                label='%d %s' % (pks.size, "valley"))
        plt.plot(t, y, 'b')
        plt.title(label)
        # plt.show()
        plt.savefig("M8Samples/BakedBeansNonLeak/"+filename + '.png')
    print("\nLeaks = "+str(leakcounter)+"\nnoLeaks = " +str(nonleakcounter) )


