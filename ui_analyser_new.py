# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'scanning.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!
import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtWidgets import QMessageBox, QDialog, QDesktopWidget, QMainWindow, QFileDialog

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from ui_infoInput import *
import pandas as pd
import matplotlib.pyplot as plt

import random
from queue import Queue
from threading import Thread

from backend import *
import analogDiscovery as ad

from PyQt5.QtCore import QThread

_FINISH = False

class Ui_Analyser(QMainWindow):
    status = 0
    emergencyNum = 30
    start = False

    df = pd.DataFrame([[0, 0]], columns=['Timestemp', 'Leak'])
    counter = 0
    thread_q = Queue()

    # def __init__(self):
    #     self.app =  QtWidgets.QApplication(sys.argv)

    def setupUi(self, Analyser):
        print("in setup UI")
        Analyser.setObjectName("Analyser")
        Analyser.resize(800, 600)
        print("after resize")
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Analyser.sizePolicy().hasHeightForWidth())
        Analyser.setSizePolicy(sizePolicy)
        Analyser.setUnifiedTitleAndToolBarOnMac(False)
        self.centralwidget = QtWidgets.QWidget(Analyser)
        self.centralwidget.setObjectName("centralwidget")

        # font for status
        status_font = QtGui.QFont()
        status_font.setFamily("Monospace")
        status_font.setPointSize(17)
        status_font.setBold(True)
        status_font.setWeight(75)

        # Progress bar
        self.progressValue = 0
        self.progressBar = QtWidgets.QProgressBar(self.centralwidget)
        self.progressBar.setGeometry(QtCore.QRect(40, 450, 441, 23))
        self.progressBar.setProperty("value", self.progressValue)
        self.progressBar.setObjectName("progressBar")

        self.scanning = QtWidgets.QLabel(self.centralwidget)
        self.scanning.setGeometry(QtCore.QRect(40, 405, 251, 31))
        self.scanning.setFont(status_font)
        self.scanning.setTextFormat(QtCore.Qt.PlainText)
        self.scanning.setObjectName("scanning")

        self.status = QtWidgets.QLabel(self.centralwidget)
        self.status.setGeometry(QtCore.QRect(100, 355, 221, 60))
        self.status.setObjectName("status")

        self.leak = QtWidgets.QLabel(self.centralwidget)
        self.leak.setGeometry(QtCore.QRect(200, 363, 221, 31))
        self.leak.setFont(status_font)
        self.leak.setTextFormat(QtCore.Qt.PlainText)
        self.leak.setObjectName("leak")
        self.leak.hide()

        # plot product signal
        self.plot_widget = QtWidgets.QWidget(self.centralwidget)
        self.plot_widget.setGeometry(10, -65, 650, 520)

        l = QtWidgets.QVBoxLayout()
        self.dc = MyDynamicMplCanvas(self.plot_widget, width=4.8, height=4, dpi=100)
        l.addWidget(self.dc)
        self.plot_widget.setLayout(l)

        # right panel

        # font for buttons
        font = QtGui.QFont()
        font.setFamily("Monospace")
        font.setPointSize(15)
        font.setBold(True)
        font.setUnderline(False)
        font.setWeight(75)

        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(520, 10, 271, 561))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")

        # display current time
        self.curTime = QtWidgets.QLabel(self.frame)
        self.curTime.setGeometry(QtCore.QRect(0, 520, 271, 31))
        self.curTime.setAlignment(QtCore.Qt.AlignCenter)
        self.curTime.setObjectName("curTime")

        # Product Info
        self.productInfo = QtWidgets.QLabel(self.frame)
        self.productInfo.setGeometry(QtCore.QRect(10, 10, 271, 31))
        self.productInfo.setFont(font)
        self.productInfo.setObjectName("productInfo")

        self.productName = QtWidgets.QLabel(self.frame)
        self.productName.setGeometry(QtCore.QRect(30, 50, 271, 31))
        self.productName.setObjectName("productName")
        self.productName.setFont(font)

        self.changeProductBtn = QtWidgets.QPushButton(self.frame)
        self.changeProductBtn.setGeometry(QtCore.QRect(120, 50, 130, 31))
        self.changeProductBtn.setObjectName("restartBtn")
        self.changeProductBtn.clicked.connect(self.openInfoInput)

        self.systemInfo = QtWidgets.QLabel(self.frame)
        self.systemInfo.setGeometry(QtCore.QRect(10, 110, 271, 31))
        self.systemInfo.setFont(font)
        self.systemInfo.setObjectName("systemInfo")

        self.curVolt = QtWidgets.QLabel(self.frame)
        self.curVolt.setGeometry(QtCore.QRect(30, 150, 271, 17))
        self.curVolt.setObjectName("curVolt")

        self.lineRate = QtWidgets.QLabel(self.frame)
        self.lineRate.setGeometry(QtCore.QRect(30, 170, 271, 40))
        self.lineRate.setObjectName("lineRate")

        self.lineRateBtn = QtWidgets.QPushButton(self.frame)
        self.lineRateBtn.setGeometry(QtCore.QRect(45, 220, 200, 31))
        self.lineRateBtn.setAutoFillBackground(False)
        self.lineRateBtn.setObjectName("lineRateBtn")
        self.lineRateBtn.clicked.connect(self.lineRateSetting)

        # emergency setting button
        self.emergencyInfo = QtWidgets.QLabel(self.frame)
        self.emergencyInfo.setGeometry(QtCore.QRect(20, 300, 271, 60))
        self.emergencyInfo.setObjectName("emergencyInfo")
        self.emergencyInfo.setFont(font)

        self.emergencyBtn = QtWidgets.QPushButton(self.frame)
        self.emergencyBtn.setGeometry(QtCore.QRect(45, 370, 200, 31))
        self.emergencyBtn.setAutoFillBackground(False)
        self.emergencyBtn.setObjectName("emergencyBtn")
        self.emergencyBtn.clicked.connect(self.emergencySetting)

        button_font = QtGui.QFont()
        button_font.setFamily("Monospace")
        button_font.setPointSize(15)
        button_font.setBold(True)
        button_font.setUnderline(False)
        button_font.setWeight(75)

        pauseIcon = QIcon()
        pauseIcon.addPixmap(QPixmap("images/pause.png"), QIcon.Normal)
        self.pauseBtn = QtWidgets.QPushButton(self.centralwidget)
        self.pauseBtn.setGeometry(QtCore.QRect(70, 490, 180, 60))
        self.pauseBtn.setObjectName("pauseBtn")
        self.pauseBtn.setStyleSheet("background-color: #FFEB3B")
        self.pauseBtn.setIcon(pauseIcon)
        self.pauseBtn.setIconSize(QtCore.QSize(50,50))
        self.pauseBtn.clicked.connect(self.pause_timer)
        self.pauseBtn.setFont(button_font)

        stopIcon = QIcon()
        stopIcon.addPixmap(QPixmap("images/stop.png"), QIcon.Normal)
        self.stopBtn = QtWidgets.QPushButton(self.centralwidget)
        self.stopBtn.setGeometry(QtCore.QRect(300, 490, 180, 60))
        self.stopBtn.setObjectName("stopBtn")
        self.stopBtn.setStyleSheet("background-color: #F44336")
        self.stopBtn.setIcon(stopIcon)
        self.stopBtn.setIconSize(QtCore.QSize(50, 50))
        self.stopBtn.clicked.connect(self.save_file)
        self.stopBtn.setFont(button_font)

        Analyser.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(Analyser)
        self.statusbar.setObjectName("statusbar")
        Analyser.setStatusBar(self.statusbar)

        self.retranslateUi(Analyser)
        QtCore.QMetaObject.connectSlotsByName(Analyser)
        self.rate = 6

        t1 = Thread(target=self.update_ui)
        # t2 = Thread(target=self.beam_break_timer)


        t1.start()
        print ("UI thread ready")
        # t2.start()
        print ("ad thread ready")

        self.csv_file = "product.csv"
        self.window = Analyser
        self.window.show()

        self.set_linerate(4000)
        # t1.join()
        # t2.join()

        # self.pauseBtn.clicked.connect(self.toggleLabel)
    def set_linerate(self, linerate):
        self.progressbar_timer = QtCore.QTimer(self)
        self.progressbar_timer.start(40)

        # simulate delay for testing
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.update_time)
        self.timer.start(1000)

    def update_ui(self):
        counter = 1
        while True:
            print("in while loop: ")
            if (Ui_Analyser.thread_q.get() == 1):
                print("got 1 one from q")
                # start progress bar for scanning
                self.progressbar_timer.timeout.connect(self.updateProgressBar)
                samplelist = self.analog_discovery.ad_sample()
                folderName = 'Leaky26_20'
                try:
                    os.makedirs("MovingPouches\\" + folderName)
                except OSError as e:
                    if e.errno != os.errno.EEXIST:
                        raise
                        # time.sleep might help here
                    pass
                self.fileName = 'MovingPouches\\' + folderName + '\\' + str(counter) + '.csv'
                self.analog_discovery.write_samples(samplelist, self.fileName)
                self.analog_discovery.write_samples(samplelist, 'currPouch.csv')
                counter += 1
            if (Ui_Analyser.thread_q.get() == 0):
                print("got zero from q")
                self.toggleDisplay()
            if _FINISH:
                print ("finishing up")
                break

    def beam_break_timer(self):
        print("in beam break timer")
        while True:
            print("polling False: " + str(self.analog_discovery.poll_digitalIO0(False)))
            print("polling True: " + str(self.analog_discovery.poll_digitalIO0(True)))

            Ui_Analyser.thread_q.put(1)

            self.analog_discovery.poll_digitalIO0(False)
            print("putting 0 in the queue")

            Ui_Analyser.thread_q.put(0)
            if _FINISH:
                break

    def save_file(self):
        global _FINISH
        self.statusbar.showMessage('Waiting to close the system.')
        self.timer.stop()
        buttonReply = QMessageBox.question(self,
                                           'Program paused',
                                           "This will quit the current process. \n Current stats saved in " + self.csv_file
                                            + "\n Save to a new location?",
                                           QMessageBox.Yes, QMessageBox.No)
        if buttonReply == QMessageBox.Yes:
            print('Yes clicked.')
            self.saveFileDialog()
        _FINISH = True
        self.analog_discovery.digitalO1(False)
        self.analog_discovery.close()
        self.window.hide()

    def saveFileDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getSaveFileName(self,"QFileDialog.getSaveFileName()","","All Files (*);;csv Files (*.csv)", options=options)
        if fileName:
            print(fileName)
            Ui_Analyser.df.to_csv(fileName)
        self.analog_discovery.digitalO1(False)
        self.window.hide()

    def openInfoInput(self):
        self.timer.stop()
        # self.progress_timer.stop()
        buttonReply = QMessageBox.question(self,
                                           'Program paused',
                                           "This will quit the current process. Proceed?",
                                           QMessageBox.Yes, QMessageBox.No)
        if buttonReply == QMessageBox.Yes:
            print('Yes clicked.')
            items = ("Tomato", "Bean")
            item, okPressed = QInputDialog.getItem(self, "Select product", "Please choose a product:", items, 0, False)
            if okPressed and item:
                print(item)
                self.productName.setText(item)


    def pause_timer(self):
        self.timer.stop()
        # self.progress_timer.stop()
        self.resume_qbox()

    def resume_qbox(self):
        buttonReply = QMessageBox.question(self,
                                           'Program paused',
                                           "Resume?",
                                           QMessageBox.Yes, QMessageBox.No)
        if buttonReply == QMessageBox.Yes:
            print('Yes clicked.')
            self.resume_timer()

    def resume_timer(self):
        self.timer.start()

    def lineRateSetting(self):
        self.timer.stop()
        self.statusbar.showMessage('Setting line rate.')
        i, okPressed = QtWidgets.QInputDialog.getInt(self, "Line Rate Setting", "current line rate ft/min:", 24, 10, 70 ,3)
        if okPressed:
            print(i)
            self.resume_timer()
            self.lineRate.setText("Line Rate: " + str(i) + " ft/min")


    def emergencySetting(self):
        self.timer.stop()
        # self.progress_timer.stop()
        self.statusbar.showMessage('Setting automatic shutoff.')
        i, okPressed = QtWidgets.QInputDialog.getInt(self, "Shutting of until", "# of leaky pouches:", 4, 0, 10, 1)
        if okPressed:
            print(i)
            self.resume_timer()
            Ui_Analyser.emergencyNum = int(i)
            self.emergencyInfo.setText(str(Ui_Analyser.emergencyNum) + " leaky pouches \n until shutoff")
        # self.analog_discovery.close()

    def emergency_response(self):
        self.timer.stop()
        buttonReply = QMessageBox.question(self,
                                           'EMERGENCY!',
                                           "Signal sent to voltage supply. \n Press emergency stop button if needed!",
                                           QMessageBox.Ok, QMessageBox.No)
        self.analog_discovery.digitalO1(False)
        self.analog_discovery.close()
        self.window.hide()
        QtCore.QCoreApplication.instance().quit()

    def updateProgressBar(self):
        if self.progressValue < 100:
            self.progressValue += 1
            self.progressBar.setValue(self.progressValue)

    def update_time(self):
        # update time
        self.curTime.setText(QtCore.QDateTime.currentDateTime().toString())

        if (Ui_Analyser.counter == self.emergencyNum):
            self.emergency_response()


    def toggleDisplay(self):
        print("in toggle display")
        # reset progress bar
        self.progressValue = 0
        self.progressBar.setValue(self.progressValue)
        # self.progressbar_timer = QtCore.QTimer(self)
        # self.progressbar_timer.timeout.connect(self.updateProgressBar)
        # self.progressbar_timer.start((self.rate * 1000) / 100)

        # analyse data and update display
        t, v = read_csv()
        y = reduce_noise(t, v)
        self.statusbar.showMessage('Data received by analyser.')
        self.dc.update_figure(t, y)
        print("figure updated")


        pks = find_peaks(y)
        vys = find_valleys(y)

        # three status:
        # 0 - Scanning
        # 1 - Leak
        # 2 - Pass
        prev_status = Ui_Analyser.status
        self.statusbar.showMessage('Analysing Pouch signal...')
        isleak = isLeak(t, y, pks, vys)
        if (isleak):
            try:
                os.rename(self.fileName, self.fileName.split('.')[0] + "_leak.csv")
            except OSError as e:
                if e.errno != os.errno.EEXIST:
                    raise
                    # time.sleep might help here
                os.remove(self.fileName.split('.')[0] + "_leak.csv")
                os.rename(self.fileName, self.fileName.split('.')[0] + "_leak.csv")
            status = 1/home/sarah/fullstack_final_project
        else:
            try:
                os.rename(self.fileName, self.fileName.split('.')[0] + "_non.csv")
            except OSError as e:
                if e.errno != os.errno.EEXIST:
                    raise
                    # time.sleep might help here
                os.remove(self.fileName.split('.')[0] + "_non.csv")
                os.rename(self.fileName, self.fileName.split('.')[0] + "_non.csv")
            status = 2



        new_df = pd.DataFrame([[QtCore.QDateTime.currentDateTime().toString(), str(isleak)]],
                              columns=['Timestemp', 'Leak'])
        Ui_Analyser.df = Ui_Analyser.df.append(new_df)

        if prev_status != status:
            if status == 1:
                self.scanning.hide()
                self.leak.setText("Leak!")
                self.leak.show()
                pixmap = QPixmap('thumbdown.png')
                pixmap = pixmap.scaled(50, 50)
                self.status.setPixmap(pixmap)
                self.resize(pixmap.width(), pixmap.height())
                self.status.show()
                Ui_Analyser.counter += 1
            elif status == 2:
                self.scanning.hide()
                self.leak.setText("Non Leak")
                self.leak.show()
                pixmap = QPixmap('thumbup.png')
                pixmap = pixmap.scaled(50, 50)
                self.status.setPixmap(pixmap)
                Ui_Analyser.counter = 0
        Ui_Analyser.df.to_csv(self.csv_file+"", mode='a')
        self.statusbar.showMessage('Waiting for next pouch signal...')


        # timerScreen = QtCore.QTimer(self)
        # timerScreen.setInterval(500)
        # timerScreen.setSingleShot(True)
        # timerScreen.timeout.connect(self.back_to_scanning)

    def back_to_scanning(self):
        print("in back to scanning")
        self.scanning.show()
        self.leak.hide()
        self.status.hide()
        Ui_Analyser.status = 0

    def set_parameters(self, product, linerate):
        print("in set parameters")
        self.productName.setText(product)
        self.name = product
        self.lineRate.setText("Line Rate: " + linerate + " ft/min")
        self.rate = float(linerate)
        self.csv_file = product + ".csv"

    def retranslateUi(self, Scanning):
        _translate = QtCore.QCoreApplication.translate
        Scanning.setWindowTitle(_translate("MainWindow", "Furmano\'s Inline Leak Detector"))
        self.scanning.setText(_translate("MainWindow", "Scanning..."))
        self.leak.setText(_translate("MainWindow", "LEAK!"))
        # self.status.setText(_translate("MainWindow", ""))
        self.curTime.setText(_translate("MainWindow", "Current time"))
        self.productInfo.setText(_translate("MainWindow", "Product Info:"))
        self.systemInfo.setText(_translate("MainWindow", "System Info:"))
        self.curVolt.setText(_translate("MainWindow", "Current Voltage:"))
        self.lineRate.setText(_translate("MainWindow", "Line Rate"))
        self.emergencyInfo.setText(_translate("MainWindow", str(Ui_Analyser.emergencyNum) + " leaky pouches \n until auto shutoff"))
        self.productName.setText(_translate("MainWindow", "Tomato"))
        self.pauseBtn.setText(_translate("MainWindow", "PAUSE"))
        self.changeProductBtn.setText(_translate("MainWindow", "Change Product"))
        self.stopBtn.setText(_translate("MainWindow", "STOP"))
        self.emergencyBtn.setText(_translate("MainWindow", "Auto Shutdown Settings"))
        self.lineRateBtn.setText(_translate("MainWindow", "Line Rate Settings"))
        self.statusbar.showMessage('System started up. Be cautious of High voltage!')

    def setup_ad(self):
        my_ad = ad.ADAPI()
        my_ad.init()
        self.control_voltage = 3.75 # control voltage for high voltage supply
        my_ad.dc_out(self.control_voltage)
        self.curVolt.setText("Current Voltage: " + str((self.control_voltage / 5.0) * 6.0) + "kV")
        my_ad.digitalO1(True)
        my_ad.ad_sample_prep(6.0)
        self.analog_discovery = my_ad

class MyMplCanvas(FigureCanvas):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""

    def __init__(self, parent=None, width=6, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)

        self.compute_initial_figure()

        FigureCanvas.__init__(self, fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                                   QtWidgets.QSizePolicy.Fixed,
                                   QtWidgets.QSizePolicy.Fixed)
        FigureCanvas.updateGeometry(self)

    def compute_initial_figure(self):
        pass


class MyDynamicMplCanvas(MyMplCanvas):
    """A canvas that updates itself every second with a new plot."""

    def __init__(self, *args, **kwargs):
        MyMplCanvas.__init__(self, *args, **kwargs)
        # timer = QtCore.QTimer(self)
        # timer.timeout.connect(self.update_figure)
        # timer.start(2500)

    def compute_initial_figure(self):
        y = np.zeros(1000)
        self.axes.set_ylim(top=2.5)
        self.axes.set_ylabel("voltage (V)")
        self.axes.set_xlabel("time (msec)")
        self.axes.plot(y, 'r')

    def update_figure(self, x, y):
        self.axes.cla()
        # x, y = read_csv()
        print ("time signal")
        print (x)
        print ("voltage signal")
        print (y)
        self.axes.set_ylim(top=2.5)
        self.axes.set_ylabel("voltage (V)")
        self.axes.set_xlabel("time (msec)")
        self.axes.plot(x, y, 'r')
        self.draw()



if __name__ == "__main__":
    import sys
    #Set up Analog Discovery
    import analogDiscovery as ad
    # my_ad = ad.ADAPI()
    #
    # my_ad.init()
    # # set analog discovery with 2.8 dc output
    # my_ad.dc_out(2.8)
    # my_ad.digitalO1(True)

    app = QtWidgets.QApplication(sys.argv)
    analyser = QtWidgets.QMainWindow()
    ui = Ui_Analyser()
    ui.setupUi(analyser)
    # ui.setup_ad()

    # analyser.setObjectName("Analyser")
    # analyser.resize(800, 600)
    # analyser.setUnifiedTitleAndToolBarOnMac(False)
    # ui.centralwidget = QtWidgets.QWidget(analyser)
    # analyser.setCentralWidget(ui.centralwidget)
    # ui.statusbar = QtWidgets.QStatusBar(analyser)
    # analyser.setStatusBar(ui.statusbar)
    #
    #
    # QtCore.QMetaObject.connectSlotsByName(analyser)

    t1 = Thread(target=ui.update_ui)
    # t2 = Thread(target=ui.beam_break_timer)

    t1.start()
    t2.start()
    ui.retranslateUi(analyser)

    # ui.setup_ad()
    # ui.setupUi(analyser)

    analyser.show()
    sys.exit(app.exec_())
