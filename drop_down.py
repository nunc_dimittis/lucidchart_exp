import sys
from PyQt5.QtWidgets import QApplication, QWidget, QInputDialog, QLineEdit, QMessageBox
from PyQt5.QtGui import QIcon


class App(QWidget):

    def __init__(self):
        super().__init__()
        self.title = "Furmano's inline leak detector"
        self.left = 10
        self.top = 10
        self.width = 640
        self.height = 480
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.getChoice()

        self.show()


    def getChoice(self):
        items = ("Tomato", "Bean")
        item, okPressed = QInputDialog.getItem(self, "Product Input", "Please choose a product:", items, 0, False)
        if ok and item:
            print(item)
            self.searchProduct(item)

    def searchProduct(self, item):
        # MainWindow.setGeometry(self.left, self.top, self.width, self.height)
        # MainWindow._center()
        if item == "Tomato":
            buttonReply = QMessageBox.question(self,
                                               'Product Code Entered',
                                               "You Entered: " + productCode + "\nProduct: Tomato found.\n Proceed?",
                                               QMessageBox.Yes | QMessageBox.No, QMessageBox.Yes)

            product = productCode + " Tomato"
            voltage = "5 kV"
            linerate = "4 sec/pouch\n         15 pouch/min"
            if buttonReply == QMessageBox.Yes:
                print('Yes clicked.')
                self.openAnalyser(product, voltage, linerate)
            else:
                print('No clicked.')

        if item == "Bean":
            buttonReply = QMessageBox.question(self,
                                               'Product Code Entered',
                                               "You Entered: " + productCode + "\nProduct: Bean found.\n Proceed?",
                                               QMessageBox.Yes | QMessageBox.No, QMessageBox.Yes)

            product = productCode + " Bean"
            voltage = "5 kV"
            linerate = "4 sec/pouch\n         15 pouch/min"
            if buttonReply == QMessageBox.Yes:
                print('Yes clicked.')
                self.openAnalyser(product, voltage, linerate)
            else:
                print('No clicked.')





if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())