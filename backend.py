import glob
import os

import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
from scipy.signal import butter, lfilter
import pandas

from peakdetect import detect_peaks
import random

# allFiles = glob.glob('signal_processing/Beans/*.csv')


def read_csv(f = "currPouch.csv"):
    # f = "currPouch.csv"
    df = pandas.read_csv(f)
    # f = random.choice(allFiles)
    # df = pandas.read_csv(f, skiprows=5, index_col='Sample Number')
    filename = os.path.splitext(os.path.basename(f))[0]
    print(filename)
    t = df.index.values
    v = (df.values).ravel()
    return t, v

def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    print(normal_cutoff)
    if normal_cutoff > 1:
        normal_cutoff = 0.9
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a

def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y

def find_peaks(v):
    ind = detect_peaks(v, mph=.8, mpd=700, show=False)
    return ind

def find_valleys(v):
    ind = detect_peaks(v, mph=-0.4, mpd=500, edge='rising', valley=True, show=False)
    return ind


def isLeak(t, y, pks, vys):
    if pks.size != 0:
        for pk in pks:
            vys = [i for i in vys if i > pk]
            if len(vys) == 0:
                vys.append(len(y) - 1)

            # if pk <= vys[0]:
            #     pass
            else:
                t1 = t[pk:vys[0]]
                y1 = y[pk:vys[0]]
                print(str(pk) + " ," + str(vys[0]))
                peak_width = t[vys[0]] - t[pk]
                print(peak_width)
                if peak_width <= 1900 and peak_width > 150:
                    print ('k-means yes')
                    return True
    return False

def reduce_noise(t, v):
    cutoff = 40.0
    Ts = t[2] - t[1]  # sampling interval
    fs = 1 / Ts

    y = butter_lowpass_filter(v, cutoff, fs, order=5)
    return y



def main():
    # Actual parameters
    A0, K0, C0 = 2.5, -4.0, 2.0

    # Generate some data based on these

    # t, v = read_csv()
    for f in allFiles:

        df = pandas.read_csv(f, skiprows=5, index_col='Sample Number')
        filename = os.path.splitext(os.path.basename(f))[0]
        print(filename)
        t = df['Time (s)'].values
        v = df['2 (VOLT)'].values

        y = reduce_noise(t, v)

        pks = find_peaks(y)
        vys = find_valleys(y)

        print(isLeak(pks, vys))

        plt.plot(t, y)
        plt.show()

def model_func(t, A, K, C):
    return A * np.exp(K * t) + C

def fit_exp_linear(t, y, C=0):
    y = y - C
    y = np.log(y)
    K, A_log = np.polyfit(t, y, 1)
    A = np.exp(A_log)
    return A, K

def fit_exp_nonlinear(t, y):
    opt_parms, parm_cov = sp.optimize.curve_fit(model_func, t, y, maxfev=1000)
    A, K, C = opt_parms
    return A, K, C

def plot(ax, t, y, noisy_y, fit_y, fit_parms):
    A, K, C = fit_parms

    ax.plot(t, y, 'k--',
            label='Actual Function:\n')
    ax.plot(t, fit_y, 'b-',
            label='Fitted Function:\n $y = %0.2f e^{%0.2f t} + %0.2f$' % (A, K, C))
    ax.plot(t, noisy_y, 'ro')
    ax.legend(bbox_to_anchor=(1.05, 1.1), fancybox=True, shadow=True)


if __name__ == '__main__':
    # main()
    read_csv()
