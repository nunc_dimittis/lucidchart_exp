import glob
import os
import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
import pandas
from scipy.signal import butter, lfilter
from peakdetect import detect_peaks
import random



allFiles = glob.glob('Beans/*.csv')

def read_csv():
    f = random.choice(allFiles)
    df = pandas.read_csv(f, skiprows=5, index_col='Sample Number')
    filename = os.path.splitext(os.path.basename(f))[0]
    print(filename)
    t = df['Time (s)'].values
    v = df['2 (VOLT)'].values

    return t, v

def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a

def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y

def find_peaks(v):
    ind = detect_peaks(v, mph=0.4, mpd=0, show=False)
    return ind

def find_valleys(v):
    ind = detect_peaks(v, mph=0, mpd=2, edge='rising', valley=True, show=False)
    return ind


def isLeak(pks, vys):
    if pks.size != 0:
        vys = [i for i in vys if i > pks[0]]
        if len(vys) == 0:
            vys.append(len(y) - 1)

        t1 = t[pks[0]:vys[0]]
        y1 = y[pks[0]:vys[0]]
        print(pks)
        print(vys)
        peak_width = t[vys[0]] - t[pks[0]]
        print(peak_width)
        if peak_width <= 0.110 and peak_width > 0.02:
            return True
        else:
            return False
    else:
        return False

def reduce_noise(t, v):
    cutoff = 40.0
    Ts = t[2] - t[1]  # sampling interval
    fs = 1 / Ts

    y = butter_lowpass_filter(v, cutoff, fs, order=5)
    return y


def main():
    # Actual parameters
    A0, K0, C0 = 2.5, -4.0, 2.0

    # Generate some data based on these

    # t, v = read_csv()
    for f in allFiles:

        df = pandas.read_csv(f, skiprows=5, index_col='Sample Number')
        filename = os.path.splitext(os.path.basename(f))[0]
        print(filename)
        t = df['Time (s)'].values
        v = df['2 (VOLT)'].values

        y = reduce_noise(t, v)

        pks = find_peaks(y)
        vys = find_valleys(y)


        plt.plot(t, y)


        plt.show()



    # tmin, tmax = 0, 0.5
    # num = 20
    # t = np.linspace(tmin, tmax, num)
    # y = model_func(t, A0, K0, C0)
    #
    # # Add noise
    # noisy_y = y + 0.5 * (np.random.random(num) - 0.5)
    #

    # fig = plt.figure()
    # ax1 = fig.add_subplot(211)
    # ax2 = fig.add_subplot(212)


    # # Non-linear Fit
    t1 = t[pks[0]:vys[0]]
    y1 = y[pks[0]:vys[0]]
    print(t[vys[0]] - t[pks[0]])
    # A, K, C = fit_exp_nonlinear(t1, y1)
    # fit_y = model_func(t1, A, K, C)
    # plot(ax1, t1, y1, y1, fit_y, (A, K, C0))
    # ax1.set_title('Non-linear Fit')

    # # Linear Fit (Note that we have to provide the y-offset ("C") value!!
    # A, K = fit_exp_linear(t1, y1, 0.01)
    # fit_y = model_func(t1, A, K, 0.01)
    # plot(ax2, t1, y1, y1, fit_y, (A, K, 0))
    # ax2.set_title('Linear Fit')

    # plt.show()

def model_func(t, A, K, C):
    return A * np.exp(K * t) + C

def fit_exp_linear(t, y, C=0):
    y = y - C
    y = np.log(y)
    K, A_log = np.polyfit(t, y, 1)
    A = np.exp(A_log)
    return A, K

def fit_exp_nonlinear(t, y):
    opt_parms, parm_cov = sp.optimize.curve_fit(model_func, t, y, maxfev=1000)
    A, K, C = opt_parms
    return A, K, C

def plot(ax, t, y, noisy_y, fit_y, fit_parms):
    A, K, C = fit_parms

    ax.plot(t, y, 'k--',
            label='Actual Function:\n')
    ax.plot(t, fit_y, 'b-',
            label='Fitted Function:\n $y = %0.2f e^{%0.2f t} + %0.2f$' % (A, K, C))
    ax.plot(t, noisy_y, 'ro')
    ax.legend(bbox_to_anchor=(1.05, 1.1), fancybox=True, shadow=True)


if __name__ == '__main__':
    main()
