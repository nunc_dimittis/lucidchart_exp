import glob
import os
import pandas
import numpy as np
import matplotlib.pyplot as plt
import scipy.fftpack
from scipy import signal

allFiles = glob.glob('Beans/*.csv')


for f in allFiles:
    # read leak data
    df = pandas.read_csv(f, skiprows=5, index_col = 'Sample Number')

    filename = os.path.splitext(os.path.basename(f))[0]
    print(filename)

    t = df['Time (s)'].values
    v = df['2 (VOLT)'].values

    N = len(t) # number of samples
    Ts = t[2] - t[1] # sampling interval
    Fs = 1/Ts
    T = N * Ts

    frq = np.arange(N)/T # two sides frequency range
    frq = frq[range(int(N/2))] # one side frequency range

    # calculate fft
    vf = scipy.fftpack.fft(v)

    # and plot it

    fig, ax = plt.subplots()
    plt.subplot(211)
    plt.plot(t, v)
    plt.xlabel("time (s)")
    plt.ylabel("voltage (v)")
    plt.grid()

    # plt.subplot(312)
    #
    # # compute spectrogram
    # f, t, Sxx = signal.spectrogram(v, Fs)
    # plt.imshow(Sxx, aspect = 'auto', cmap = 'hot_r', origin = 'lower')
    # # plt.pcolormesh(t, f, Sxx)
    # plt.ylabel('Frequency [Hz]')
    # plt.xlabel('Time [sec]')

    plt.subplot(212)

    # plt.plot(tf, 2.0/N * np.abs(vf[:N//2]))
    plt.plot(frq, 2.0/N * np.abs(vf[range(int(N/2))]))
    plt.suptitle(filename)
    plt.xlabel("frequency [hz]")
    plt.ylabel("amplitude")
    plt.grid()


    # set the size of figure to 1600 * 850 (px)
    # my_dpi = fig.get_dpi()
    # fig.set_size_inches(1600/my_dpi, 850/my_dpi)
    # plt.savefig(filename + '.png')

    plt.show()
