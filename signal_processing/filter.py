# http://scipy.github.io/old-wiki/pages/Cookbook/ButterworthBandpass
from scipy.signal import butter, lfilter


def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a


def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y


def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a

def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y


if __name__ == "__main__":
    import glob
    import os
    import pandas
    import numpy as np
    import matplotlib.pyplot as plt
    from scipy.signal import freqz
    import scipy.fftpack


    allFiles = glob.glob('Beans/*.csv')


    for f in allFiles:
        # read leak data
        df = pandas.read_csv(f, skiprows=5, index_col = 'Sample Number')

        filename = os.path.splitext(os.path.basename(f))[0]
        print(filename)

        t = df['Time (s)'].values
        v = df['2 (VOLT)'].values

        # Sample rate and desired cutoff frequencies (in Hz).
        N = len(t) # number of samples
        Ts = t[2] - t[1] # sampling interval
        fs = 1/Ts
        T = N * Ts
        #fs = 5000.0
        # lowcut = 5.0
        # highcut = 40.0
        cutoff = 40.0

        frq = np.arange(N)/T # two sides frequency range
        frq = frq[range(int(N/2))] # one side frequency range

        # calculate fft
        vf = scipy.fftpack.fft(v)

        # Plot the frequency response for a few different orders.
        fig1 = plt.figure(1)
        plt.clf()
        for order in [3, 5]:
            b, a = butter_lowpass(cutoff, fs, order=order)
            w, h = freqz(b, a, worN=2000)
            plt.plot((fs * 0.5 / np.pi) * w, abs(h), label="order = %d" % order)

        plt.plot(frq, 10.0/N * np.abs(vf[range(int(N/2))]), '--', label = 'fft response')
        # plt.plot([0, 0.5 * fs], [np.sqrt(0.5), np.sqrt(0.5)],
        #         '--', label='sqrt(0.5)')
        plt.xlabel('Frequency (Hz)')
        plt.ylabel('Gain')
        plt.grid(True)
        plt.legend(loc='best')

        # set the size of figure to 1600 * 850 (px)
        my_dpi = fig1.get_dpi()
        fig1.set_size_inches(1600/my_dpi, 850/my_dpi)
        plt.savefig('freqresp_' + filename + '.png')

        fig2 = plt.figure(2)
        plt.clf()
        plt.plot(t, v, label='Noisy signal')

        y = butter_lowpass_filter(v, cutoff, fs, order=3)
        plt.plot(t, y, label='Filtered signal (Hz)')
        plt.xlabel('time (seconds)')
        plt.grid(True)
        plt.axis('tight')
        plt.legend(loc='upper left')

        # set the size of figure to 1600 * 850 (px)
        my_dpi = fig2.get_dpi()
        fig2.set_size_inches(1600/my_dpi, 850/my_dpi)
        plt.savefig('filtered_' + filename + '.png')

        # plt.show()
